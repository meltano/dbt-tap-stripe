# dbt | tap-stripe

This [dbt](https://github.com/fishtown-analytics/dbt) package contains data models for [tap-stripe](https://github.com/meltano/tap-stripe).
