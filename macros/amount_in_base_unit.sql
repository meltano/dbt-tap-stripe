{%- macro amount_in_base_unit (column_name, currency_column_name) -%}
    case
        -- List of zero-decimal currencies taken from https://stripe.com/docs/currencies#zero-decimal
        when
            {{ currency_column_name }} in ('bif', 'clp', 'djf', 'gnf', 'jpy', 'kmf', 'krw', 'mga', 'pyg', 'rwf', 'ugx', 'vnd', 'vuv', 'xaf', 'xof', 'xpf')
        then
            coalesce({{ column_name }}, 0)::numeric
        else
            round(coalesce({{ column_name }}, 0)::numeric / 100, 2)
    end
{%- endmacro -%}