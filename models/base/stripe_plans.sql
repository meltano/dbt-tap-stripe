with plans as (

  select * from {{var('schema')}}.plans

),

renamed as (

    select

        id as plan_id,

        name as name,
        nickname as nickname,

        {{ tap_stripe.amount_in_base_unit('amount', 'currency') }} as amount,
        currency as currency,

        interval as plan_interval,
        interval_count as interval_count,
        case
            when interval = 'week'
            then
                {{ tap_stripe.amount_in_base_unit('amount::numeric * 52 / 12', 'currency') }}
            when interval = 'month'
            then
                {{ tap_stripe.amount_in_base_unit('amount', 'currency') }}
            when interval = 'year'
            then
                {{ tap_stripe.amount_in_base_unit('amount::numeric / 12', 'currency') }}
        end as plan_mrr_amount,

        billing_scheme as billing_scheme,
        usage_type as usage_type,

        created as created_at,
        updated as updated_at

	from plans

    {% if var('livemode') %}

        where livemode = true

    {% endif %}
)

select * from renamed
