with invoices as (

  select * from {{var('schema')}}.invoices

),

renamed as (

    select

        id as invoice_id,

        nullif(subscription, '') as subscription_id,
        nullif(charge, '') as charge_id,
        nullif(customer, '') as customer_id,

        "number" as invoice_number,

        date as invoice_date,
        period_start as period_start,
        period_end as period_end,

        currency as currency,

        {{ tap_stripe.amount_in_base_unit('total', 'currency') }} as total,
        {{ tap_stripe.amount_in_base_unit('subtotal', 'currency') }} as subtotal,

        {{ tap_stripe.amount_in_base_unit('amount_due', 'currency') }} as amount_due,
        {{ tap_stripe.amount_in_base_unit('amount_paid', 'currency') }} as amount_paid,
        {{ tap_stripe.amount_in_base_unit('amount_remaining', 'currency') }} as amount_remaining,

        --Taken from fishtown-analytics/stripe:
        -- sometimes forgiven is null but it's clear that it shouldn't be
        -- and we can infer the value (we can't always infer successfully)
        -- this only happens to records prior to 2015.
        case
            when forgiven is not null then forgiven
            else
                case when paid = true then false end
        end as forgiven,


        attempt_count as attempt_count,
        attempted as attempted,
        closed as closed,
        next_payment_attempt as next_payment_attempt,
        paid as paid,

        billing as billing,
        billing_reason as billing_reason,

        case
            when discount__coupon__amount_off is null 
              and discount__coupon__percent_off is null then null
            when discount__coupon__percent_off is null then 'amount'
            else 'percent'
        end as discount_type,

        coalesce(discount__coupon__percent_off, 
                {{ tap_stripe.amount_in_base_unit('discount__coupon__amount_off', 'currency') }}) as discount_value,

        discount__start as discount_start,
        discount__end as discount_end,

        updated as updated_at

    from invoices

    {% if var('livemode') %}
    
        where livemode = true
    
    {% endif %}
)

select * from renamed
