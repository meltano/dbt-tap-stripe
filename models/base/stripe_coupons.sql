with coupons as (

  select * from {{var('schema')}}.coupons

),

renamed as (

    select

        id as coupon_id,

        name as coupon_name,

        {{ tap_stripe.amount_in_base_unit('amount_off', 'currency') }} as amount_discount,
        currency as currency,

        percent_off as percent_discount,

        duration as duration,
        duration_in_months as duration_in_months,

        max_redemptions as max_redemptions,
        redeem_by as redeem_by,
        times_redeemed as times_redeemed,

        valid as valid,

        created as created_at,
        updated as updated_at
        
    from coupons

  {% if var('livemode') %}
  
    where livemode = true
  
  {% endif %}
)

select * from renamed
