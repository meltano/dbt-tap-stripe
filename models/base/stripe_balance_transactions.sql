with balance_transactions as (

  select * from {{var('schema')}}.balance_transactions

),

renamed as (

    select

        id as balance_transaction_id,

        source as source_id,

		{{ tap_stripe.amount_in_base_unit('amount', 'currency') }} as gross_amount,
		{{ tap_stripe.amount_in_base_unit('fee', 'currency') }} as fee,
		{{ tap_stripe.amount_in_base_unit('net', 'currency') }} as net_amount,
		currency as currency,
		exchange_rate as exchange_rate,

		status as status,
		type as type,

		description as description,

        -- Add date related columns for categorization
        created as created_at,
        EXTRACT(HOUR FROM created) created_hour,
        EXTRACT(DAY FROM created) created_day,
        EXTRACT(ISODOW FROM created) created_day_of_week,
        EXTRACT(WEEK FROM created) created_week,
        EXTRACT(MONTH FROM created) created_month,
        EXTRACT(QUARTER FROM created) created_quarter, 
        EXTRACT(YEAR FROM created) created_year,
        EXTRACT(ISOYEAR FROM created) created_iso_year,

        updated as updated_at


    from balance_transactions

)

select * from renamed
