select

		id,
		subscription,
		plan__id

from {{var('schema')}}.subscription_items

{% if var('livemode') %}

	where livemode = true

{% endif %}
