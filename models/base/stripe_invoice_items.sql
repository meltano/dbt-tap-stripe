with invoice_items as (

  select * from {{var('schema')}}.invoice_items

),

renamed as (

    select

        id as invoice_item_id,

        nullif(invoice, '') as invoice_id,
        nullif(customer, '') as customer_id,

        nullif(subscription, '') as subscription_id,
        nullif(subscription_item, '') as subscription_item_id,

        date as invoice_date,
        period__start as period_start,
        period__end as period_end,

        proration as proration,
        plan__id as plan_id,

        {{ tap_stripe.amount_in_base_unit('amount', 'currency') }} as amount,
        currency as currency,

        quantity as quantity,

        description as description,

        updated as updated_at

    from invoice_items

    {% if var('livemode') %}
    
        where livemode = true
    
    {% endif %}
)

select * from renamed
