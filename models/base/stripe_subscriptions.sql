with subscriptions as (

  select * from {{var('schema')}}.subscriptions

),

renamed as (

    select

        id as subscription_id,
        customer as customer_id,

        status as status,
        
        start as start,
        current_period_start as period_start,
        current_period_end as period_end,
        canceled_at as canceled_at,

        quantity as quantity,

        plan__id as plan_id,
        coalesce(plan__name, plan__nickname) as plan_name,
        plan__interval as plan_interval,
        {{ tap_stripe.amount_in_base_unit('plan__amount', 'plan__currency') }} as plan_amount,
        plan__currency as plan_currency,

        billing as billing,
        billing_cycle_anchor as billing_cycle_anchor,
        days_until_due as days_until_due,

        case
            when discount__coupon__amount_off is null 
              and discount__coupon__percent_off is null then null
            when discount__coupon__percent_off is null then 'amount'
            else 'percent'
        end as discount_type,

        coalesce(discount__coupon__percent_off,
                {{ tap_stripe.amount_in_base_unit('discount__coupon__amount_off', 'discount__coupon__currency') }}) as discount_value,

        discount__start as discount_start,
        discount__end as discount_end,

        created as created_at,
        updated as updated_at


    from subscriptions

    {% if var('livemode') %}
    
        where livemode = true
    
    {% endif %}
)

select * from renamed
