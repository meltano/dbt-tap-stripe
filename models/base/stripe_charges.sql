with charges as (

  select * from {{var('schema')}}.charges

),

renamed as (

    select

        id as charge_id,

		customer as customer_id,
		invoice as invoice_id,
		balance_transaction as balance_transaction_id,

		{{ tap_stripe.amount_in_base_unit('amount', 'currency') }} as amount,
		{{ tap_stripe.amount_in_base_unit('amount_refunded', 'currency') }} as amount_refunded,
        {{ tap_stripe.amount_in_base_unit('coalesce(amount, 0) - coalesce(amount_refunded, 0)', 'currency') }} as net_amount,
		currency as currency,

        case
            when (amount > 0) and (amount_refunded = amount)
                then True
            else False
        end as charge_refunded,

        case
            when (amount > 0) and (amount_refunded = amount)
                then 'Yes'
            else 'No'
        end as charge_refunded_txt,

        -- Value represents the total net amount in case of a non fully refunded charge
        --  or the amount refunded in case of a refunded charge
        case
            when (amount > 0) and (amount_refunded = amount)
            then
                {{ tap_stripe.amount_in_base_unit('amount_refunded', 'currency') }}
            else
                {{ tap_stripe.amount_in_base_unit('coalesce(amount, 0) - coalesce(amount_refunded, 0)', 'currency') }}
        end as value,

		outcome__network_status as outcome_network_status,
		outcome__reason as outcome_reason,
		outcome__seller_message as outcome_seller_message,
		outcome__type as outcome_type,

		paid as paid,
		statement_descriptor as statement_descriptor,
		status as status,

        -- Add date related columns for categorization
        created as created_at,
        EXTRACT(HOUR FROM created) created_hour,
        EXTRACT(DAY FROM created) created_day,
        EXTRACT(ISODOW FROM created) created_day_of_week,
        EXTRACT(WEEK FROM created) created_week,
        EXTRACT(MONTH FROM created) created_month,
        EXTRACT(QUARTER FROM created) created_quarter, 
        EXTRACT(YEAR FROM created) created_year,

        updated as updated_at,
        EXTRACT(HOUR FROM updated) updated_hour,
        EXTRACT(DAY FROM updated) updated_day,
        EXTRACT(ISODOW FROM updated) updated_day_of_week,
        EXTRACT(WEEK FROM updated) updated_week,
        EXTRACT(MONTH FROM updated) updated_month,
        EXTRACT(QUARTER FROM updated) updated_quarter, 
        EXTRACT(YEAR FROM updated) updated_year 


    from charges

	{% if var('livemode') %}
	
		where livemode = true
	
	{% endif %}
)

select * from renamed
