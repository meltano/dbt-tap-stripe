with customers as (

  select * from {{var('schema')}}.customers

),

renamed as (

    select

		id as customer_id,

		description as name,
		email as email,

		invoice_prefix as invoice_prefix,

		case
			when discount__coupon__amount_off is null 
			  and discount__coupon__percent_off is null then null
			when discount__coupon__percent_off is null then 'amount'
			else 'percent'
		end as discount_type,

		coalesce(discount__coupon__percent_off, {{ tap_stripe.amount_in_base_unit('discount__coupon__amount_off', 'currency') }}) as discount_value,

		discount__start as discount_start,
		discount__end as discount_end,

		created as created_at,
		updated as updated_at
		
    from customers

	{% if var('livemode') %}
	
		where livemode = true
	
	{% endif %}
)

select * from renamed
