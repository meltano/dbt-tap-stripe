select

		id,
		invoice_item,
		invoice,
		subscription,
		subscription_item,
		plan__id,
		currency,

		{{ tap_stripe.amount_in_base_unit('amount', 'currency') }} as amount

from {{var('schema')}}.invoice_line_items

{% if var('livemode') %}

	where livemode = true

{% endif %}
