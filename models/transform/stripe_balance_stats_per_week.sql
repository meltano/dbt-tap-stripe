with balance_transactions as (

     select *
     from {{ref('stripe_balance_transactions')}}

),

dates as (

     select *
     from {{ref('stripe_dates')}}

),

aggregated as (

  select

    date_trunc('week', MIN(created_at))::date as week_start,

    currency,

    COUNT(*) as transactions,
    SUM(gross_amount) as gross_amount,
    SUM(net_amount) as net_amount,
    SUM(fee) as fee

  from balance_transactions

  group by
    created_iso_year,
    created_week,
    currency

  order by
    created_iso_year,
    created_week,
    currency

)

SELECT 

  dates.date_actual as week_start,

  -- Generate a descriptive label: "[2019-12-09,2019-12-15]"
  CONCAT('[', dates.first_day_of_week, ',', dates.last_day_of_week, ']') as label,

  currency,

  COALESCE(transactions, 0) as transactions,
  COALESCE(gross_amount, 0) as gross_amount,
  COALESCE(net_amount, 0) as net_amount,
  COALESCE(fee, 0) as fee

FROM dates
LEFT OUTER JOIN aggregated
ON aggregated.week_start = dates.date_actual 
WHERE dates.day_of_week = 1