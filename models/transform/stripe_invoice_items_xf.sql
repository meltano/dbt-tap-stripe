with invoices as (

    select * from {{ref('stripe_invoices')}}

),

invoice_items as (

    select * from {{ref('stripe_invoice_items')}}

),

joined as (

    select

        invoice_items.*,

        --Following fishtown-analytics/stripe:
        case
            when invoices.discount_type is null then null
            when invoices.discount_type = 'percent'
                then round(amount * (1.0 - invoices.discount_value::numeric / 100), 2)
            else amount - invoices.discount_value
        end as discounted_amount

    from invoice_items

    left outer join invoices
        on invoice_items.invoice_id = invoices.invoice_id
        and invoice_items.invoice_date > invoices.discount_start
        and (invoice_items.invoice_date < invoices.discount_end
             or invoices.discount_end is null)

),

final as (

    select

        invoice_item_id,
        invoice_id,
        customer_id,
        subscription_id,
        subscription_item_id,
        invoice_date,
        period_start,
        period_end,
        proration,
        plan_id,
        amount,
        coalesce(discounted_amount, amount) as discounted_amount,
        currency,
        quantity,
        description,
        updated_at

    from joined

)

select * from final
