with balance_transactions as (

     select *
     from {{ref('stripe_balance_transactions')}}

),

dates as (

     select *
     from {{ref('stripe_dates')}}

),

aggregated as (

  select

    DATE(MIN(created_at)) as created_date,

    currency,

    COUNT(*) as transactions,
    SUM(gross_amount) as gross_amount,
    SUM(net_amount) as net_amount,
    SUM(fee) as fee

  from balance_transactions

  group by
    created_year,
    created_month,
    created_day,
    currency

  order by
    created_year,
    created_month,
    created_day,
    currency

)

SELECT 

  dates.date_actual as created_date,

  currency,

  COALESCE(transactions, 0) as transactions,
  COALESCE(gross_amount, 0) as gross_amount,
  COALESCE(net_amount, 0) as net_amount,
  COALESCE(fee, 0) as fee

FROM dates
LEFT OUTER JOIN aggregated
ON aggregated.created_date = dates.date_actual