with balance_transactions as (

     select *
     from {{ref('stripe_balance_transactions')}}

),

dates as (

     select *
     from {{ref('stripe_dates')}}

),

aggregated as (

  select

    date_trunc('month', MIN(created_at))::date as month_start,

    currency,

    COUNT(*) as transactions,
    SUM(gross_amount) as gross_amount,
    SUM(net_amount) as net_amount,
    SUM(fee) as fee

  from balance_transactions

  group by
    created_year,
    created_month,
    currency

  order by
    created_year,
    created_month,
    currency

)

SELECT 

  dates.date_actual as month_start,

  to_char(dates.date_actual, 'YYYY-MM') as month,

  currency,

  COALESCE(transactions, 0) as transactions,
  COALESCE(gross_amount, 0) as gross_amount,
  COALESCE(net_amount, 0) as net_amount,
  COALESCE(fee, 0) as fee

FROM dates
LEFT OUTER JOIN aggregated
ON aggregated.month_start = dates.date_actual 
WHERE dates.day_of_month = 1